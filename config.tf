provider "aws" {
  region = "us-east-1"
  access_key =""
  secret_key = ""
}

resource "aws_elastic_beanstalk_application" "javaapp" {
  name = "javaapp"
  description ="java spring boot applications"
}

output "arn" {
  value = "${aws_elastic_beanstalk_application.javaapp.arn}"
}

output "description" {
  value = "${aws_elastic_beanstalk_application.javaapp.description}"
}